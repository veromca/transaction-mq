package com.veromca.mqtask;

import java.util.concurrent.CountDownLatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * 消息发送系统
 * 当某个服务调用可靠性消息服务的接口时，进行消息的发送操作
 * 1.先将消息内容持久化到数据库中
 * 2.再单独通过消息系统将消息具体分发到MQ中
 * 采用redis实现分布式锁-避免在分布式环境下多个实例同时发送同样的消息导致消息重复
 */
@EnableDiscoveryClient
@EnableEurekaClient
@EnableFeignClients(basePackages = "com.veromca.mqclient")
@SpringBootApplication
public class TransactionTaskApplication {
	private static final Logger LOGGER = LoggerFactory.getLogger(TransactionTaskApplication.class);
	//开启均衡负载能力
	@Bean
	@LoadBalanced
	RestTemplate restTemplate() {
		return new RestTemplate();
	}
	public static void main(String[] args) {
		SpringApplication application = new SpringApplication(TransactionTaskApplication.class);
		ConfigurableApplicationContext content = application.run(args);
        try {
        	ProcessMessageTask task = content.getBean(ProcessMessageTask.class);
        	task.start();
        	// 倒计时协调器：1表示task 线程被调用一次，主线程被暂停等待task 子线程执行完毕
        	new CountDownLatch(1).await();
		} catch (InterruptedException e) {
			LOGGER.error("项目启动异常", e);
		}
	}
}
