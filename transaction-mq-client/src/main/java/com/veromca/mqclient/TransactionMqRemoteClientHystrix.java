package com.veromca.mqclient;

import java.util.ArrayList;
import java.util.List;

import com.veromca.mqclient.dto.TransactionMessage;
import com.veromca.mqclient.query.MessageQuery;
import org.springframework.stereotype.Component;

/**
 * 事务消息服务调用客户端
 * @author veromca
 *
 */
@Component
public class TransactionMqRemoteClientHystrix implements TransactionMqRemoteClient {

	@Override
	public boolean sendMessage(TransactionMessage message) {
		return false;
	}

	@Override
	public boolean sendMessage(List<TransactionMessage> messages) {
		return false;
	}

	@Override
	public boolean confirmCustomerMessage(String customerSystem, Integer messageId) {
		return false;
	}

	@Override
	public List<TransactionMessage> findByWatingMessage(int limit) {
		return new ArrayList<TransactionMessage>();
	}

	@Override
	public boolean confirmDieMessage(Long messageId) {
		return false;
	}

	@Override
	public boolean incrSendCount(Long messageId, String sendDate) {
		return false;
	}

	@Override
	public boolean retrySendDieMessage() {
		return false;
	}

	@Override
	public List<TransactionMessage> findMessageByPage(MessageQuery query) {
		return new ArrayList<TransactionMessage>();
	}

}
