CREATE TABLE `transaction_warehouse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_no` varchar(30) NOT NULL COMMENT '订单编号',
  `warehouse_no` varchar(30) NOT NULL COMMENT '入库单号',
  `warehouse_status` int(2) NOT NULL COMMENT '状态：0待入库确认  1已入库',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT = '入库单';
INSERT INTO `transaction_warehouse` (`id`, `order_no`, `warehouse_no`, `warehouse_status`) VALUES ('1', '1', 'N100101', '0');
