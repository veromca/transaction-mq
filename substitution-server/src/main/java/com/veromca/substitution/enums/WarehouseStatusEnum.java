package com.veromca.substitution.enums;

/**
 * 消息状态枚举类
 * @author veromca
 *
 */
public enum WarehouseStatusEnum {
	/**
	 * 待入库确认
	 */
	PENDING(0),
	
	/**
	 * 已入库
	 */
	WAREHOUSING(1);
	
	private int status;
	
	public static WarehouseStatusEnum parse(int status) {
		for (WarehouseStatusEnum stat : values()) {
			if (stat.getStatus() == status) {
				return stat;
			}
		}
		return null;
	}
	
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	private WarehouseStatusEnum(int status) {
		this.status = status;
	}
}