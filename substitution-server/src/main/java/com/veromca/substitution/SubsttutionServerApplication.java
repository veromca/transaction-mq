package com.veromca.substitution;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 消息消费端
 *
 */
@SpringBootApplication
@EnableFeignClients(basePackages = "com.veromca.mqclient")
@EnableEurekaClient
@EnableTransactionManagement
@MapperScan("com.veromca.substitution.mapper")
public class SubsttutionServerApplication {
	public static void main(String[] args) {
		 SpringApplication.run(SubsttutionServerApplication.class, args);
	}
}
