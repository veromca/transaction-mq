package com.veromca.substitution.mapper;

import com.veromca.substitution.po.WarehousePO;
import tk.mybatis.mapper.common.Mapper;

/**
 * 
 * 
 * @author veromca
 * @email 1395532033@qq.com
 * @date 2019-12-01 15:30:01
 */
public interface WarehouseMapper extends Mapper<WarehousePO> {
	public int retrySendDieMessage();
}
