package com.veromca.substitution.po;


import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;


/**
 * @author veromca
 * @email 1395532033@qq.com
 * @date 2019-12-01 15:30:01
 */
public class TransactionOrderDto implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer id;

    /**
     * 消息内容
     */
    private String orderNo;

    /**
     * 状态：0待确认  1待收货，2已收货
     */
    private Integer orderStatus ;

    /**
     * 设置：
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取：
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置：状态：0待确认  1待收货，2已收货
     */
    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    /**
     * 获取：状态：0待确认  1待收货，2已收货
     */
    public Integer getOrderStatus() {
        return orderStatus;
    }


    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }
}
