package com.veromca.substitution.service;

import com.veromca.common.biz.BaseBiz;
import com.veromca.substitution.enums.WarehouseStatusEnum;
import com.veromca.substitution.mapper.WarehouseMapper;
import com.veromca.substitution.po.TransactionOrderDto;
import com.veromca.substitution.po.WarehousePO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Description:
 * @Author: liusongqing
 * @CreateDate: 2019/12/01
 * @UpdateUser:
 * @UpdateDate:
 * @Copyright:
 * @UpdateRemark:
 * @Version: 0.2
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class WarehouseServiceImpl extends BaseBiz<WarehouseMapper, WarehousePO>  implements IWarehouseService {
    @Autowired
    private WarehouseMapper transactionMessageMapper;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean update(TransactionOrderDto dto){
        WarehousePO wp = new WarehousePO();
        wp.setId(1);
        wp.setOrderNo(dto.getId());
        wp.setWarehouseNo("N100101");
        wp.setWarehouseStatus(WarehouseStatusEnum.WAREHOUSING.getStatus());
        super.updateById(wp);
        return true;

    }

}
