package com.veromca.substitution.service;

import com.veromca.substitution.po.TransactionOrderDto;

/**
 * 消息业务类
 *
 * @author veromca
 */
public interface IWarehouseService {

    /**
     * 修改入库状态
     *
     * @param dto 消息内容
     * @return true 成功 | false 失败
     */
    boolean update(TransactionOrderDto dto);


}