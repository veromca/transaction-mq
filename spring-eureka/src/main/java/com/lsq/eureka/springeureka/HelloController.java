package com.lsq.eureka.springeureka;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @Author: liusongqing
 * @CreateDate: 2019/10/27
 * @UpdateUser:
 * @UpdateDate:
 * @Copyright:
 * @UpdateRemark:
 * @Version: 0.2
 */
@RestController
public class HelloController {
    @RequestMapping(value = "/user" ,method = RequestMethod.POST)
    public UserDto user(@RequestBody UserDto userDto) throws Exception {
        return userDto;
    }
}
