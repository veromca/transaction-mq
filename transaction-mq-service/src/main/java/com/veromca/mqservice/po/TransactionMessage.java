package com.veromca.mqservice.po;

import com.veromca.mqservice.enums.TransactionMessageStatusEnum;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;


/**
 * 
 * 
 * @author veromca
 * @email 1395532033@qq.com
 * @date 2019-12-01 15:30:01
 */
@Table(name = "transaction_message")
public class TransactionMessage implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //
    @Id
    private Integer id;
	
	    //消息内容
    @Column(name = "message")
    private String message;
	
	    //队列名称
    @Column(name = "queue")
    private String queue;
	
	    //发送消息的系统
    @Column(name = "send_system")
    private String sendSystem;
	
	    //重复发送消息次数
    @Column(name = "send_count")
    private Integer sendCount;
	
	    //创建时间
    @Column(name = "create_date")
	// 消息创建时间
	private Date createDate;
	
	    //最近发送消息时间
    @Column(name = "send_date")
    private Date sendDate;
	
	    //状态：0等待消费  1已消费  2已死亡
    @Column(name = "status")
    private Integer status = TransactionMessageStatusEnum.WATING.getStatus();
	
	    //死亡次数条件，由使用方决定，默认为发送10次还没被消费则标记死亡,人工介入
    @Column(name = "die_count")
    private Integer dieCount = 10;
	
	    //消费时间
    @Column(name = "customer_date")
    private Date customerDate;
	
	    //消费系统
    @Column(name = "customer_system")
    private String customerSystem;
	
	    //死亡时间
    @Column(name = "die_date")
    private Date dieDate;
	

	/**
	 * 设置：
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 获取：
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置：消息内容
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * 获取：消息内容
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * 设置：队列名称
	 */
	public void setQueue(String queue) {
		this.queue = queue;
	}
	/**
	 * 获取：队列名称
	 */
	public String getQueue() {
		return queue;
	}
	/**
	 * 设置：发送消息的系统
	 */
	public void setSendSystem(String sendSystem) {
		this.sendSystem = sendSystem;
	}
	/**
	 * 获取：发送消息的系统
	 */
	public String getSendSystem() {
		return sendSystem;
	}
	/**
	 * 设置：重复发送消息次数
	 */
	public void setSendCount(Integer sendCount) {
		this.sendCount = sendCount;
	}
	/**
	 * 获取：重复发送消息次数
	 */
	public Integer getSendCount() {
		return sendCount;
	}
	/**
	 * 设置：创建时间
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	/**
	 * 获取：创建时间
	 */
	public Date getCreateDate() {
		return createDate;
	}
	/**
	 * 设置：最近发送消息时间
	 */
	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}
	/**
	 * 获取：最近发送消息时间
	 */
	public Date getSendDate() {
		return sendDate;
	}
	/**
	 * 设置：状态：0等待消费  1已消费  2已死亡
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}
	/**
	 * 获取：状态：0等待消费  1已消费  2已死亡
	 */
	public Integer getStatus() {
		return status;
	}
	/**
	 * 设置：死亡次数条件，由使用方决定，默认为发送10次还没被消费则标记死亡,人工介入
	 */
	public void setDieCount(Integer dieCount) {
		this.dieCount = dieCount;
	}
	/**
	 * 获取：死亡次数条件，由使用方决定，默认为发送10次还没被消费则标记死亡,人工介入
	 */
	public Integer getDieCount() {
		return dieCount;
	}
	/**
	 * 设置：消费时间
	 */
	public void setCustomerDate(Date customerDate) {
		this.customerDate = customerDate;
	}
	/**
	 * 获取：消费时间
	 */
	public Date getCustomerDate() {
		return customerDate;
	}
	/**
	 * 设置：消费系统
	 */
	public void setCustomerSystem(String customerSystem) {
		this.customerSystem = customerSystem;
	}
	/**
	 * 获取：消费系统
	 */
	public String getCustomerSystem() {
		return customerSystem;
	}
	/**
	 * 设置：死亡时间
	 */
	public void setDieDate(Date dieDate) {
		this.dieDate = dieDate;
	}
	/**
	 * 获取：死亡时间
	 */
	public Date getDieDate() {
		return dieDate;
	}
}
