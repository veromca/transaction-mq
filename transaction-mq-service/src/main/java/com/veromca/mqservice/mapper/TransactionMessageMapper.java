package com.veromca.mqservice.mapper;

import com.veromca.mqservice.po.TransactionMessage;
import tk.mybatis.mapper.common.Mapper;

/**
 * 
 * 
 * @author veromca
 * @email 1395532033@qq.com
 * @date 2019-12-01 15:30:01
 */
public interface TransactionMessageMapper extends Mapper<TransactionMessage> {
	public int retrySendDieMessage();
}
