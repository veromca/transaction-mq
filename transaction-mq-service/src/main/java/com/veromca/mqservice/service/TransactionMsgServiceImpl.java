package com.veromca.mqservice.service;

import com.veromca.common.biz.BaseBiz;
import com.veromca.common.msg.PageQueryParam;
import com.veromca.common.util.Query;
import com.veromca.mqservice.mapper.TransactionMessageMapper;
import com.veromca.mqservice.po.TransactionMessage;
import com.veromca.mqservice.enums.TransactionMessageStatusEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @Author: liusongqing
 * @CreateDate: 2019/12/01
 * @UpdateUser:
 * @UpdateDate:
 * @Copyright:
 * @UpdateRemark:
 * @Version: 0.2
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class TransactionMsgServiceImpl extends BaseBiz<TransactionMessageMapper, TransactionMessage> implements TransactionMessageService {
    @Autowired
    private TransactionMessageMapper transactionMessageMapper;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean sendMessage(TransactionMessage message) {
        if (check(message)) {
            super.insert(message);
            return true;
        }
        return false;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean sendMessage(List<TransactionMessage> messages) {
        for (TransactionMessage message : messages) {
            if (!check(message)) {
                return false;
            }
        }
        for (TransactionMessage message : messages){
            super.insert(message);
        }
        return true;
    }

    private boolean check(TransactionMessage message) {
        if (!StringUtils.hasText(message.getMessage()) || !StringUtils.hasText(message.getQueue())
                || !StringUtils.hasText(message.getSendSystem())) {
            return false;
        }
        return true;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean confirmCustomerMessage(String customerSystem, Integer messageId) {
        TransactionMessage message = super.selectById(messageId);
        if (message == null) {
            return false;
        }
        message.setCustomerDate(new Date());
        message.setStatus(TransactionMessageStatusEnum.OVER.getStatus());
        message.setCustomerSystem(customerSystem);
        super.updateById(message);
        return true;
    }

    @Override
    public List<TransactionMessage> findByWatingMessage(int limit) {
        if (limit > 1000) {
            limit = 1000;
        }
        //查询列表数据
        Map<String,Object> params = new HashMap<String,Object>();
        params.put("page",1);
        params.put("limit",limit);
        params.put("status",TransactionMessageStatusEnum.WATING.getStatus());
        Query query = new Query(params);
        return (List<TransactionMessage>)super.selectByQuery1(query);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public boolean confirmDieMessage(Long messageId) {
        TransactionMessage message = super.selectById(messageId.intValue());
        if (message == null) {
            return false;
        }
        message.setStatus(TransactionMessageStatusEnum.DIE.getStatus());
        message.setDieDate(new Date());
        super.updateById(message);
        return true;
    }

    @Override
    public boolean incrSendCount(Long messageId, Date sendDate) {
        TransactionMessage message = super.selectById(messageId.intValue());
        if (message == null) {
            return false;
        }
        message.setSendDate(sendDate);
        message.setSendCount(message.getSendCount() + 1);
        super.updateById(message);
        return false;
    }

    @Override
    public boolean retrySendDieMessage() {
        transactionMessageMapper.retrySendDieMessage();
        return true;
    }

    @Override
    public List<TransactionMessage> findMessageByPage(PageQueryParam query, TransactionMessageStatusEnum status) {

        //查询列表数据
        Map<String,Object> params = new HashMap<String,Object>();
        params.put("page",1);
        params.put("limit", query.getLimit());
        params.put("status",status.getStatus());
        Query query1 = new Query(params);
        return super.selectByQuery1(query1);
        //return super.listForPage("status", status.getStatus(), query.getStart(),
        //        query.getLimit(), TransactionMessage.ID_ORDERS);
    }
}
