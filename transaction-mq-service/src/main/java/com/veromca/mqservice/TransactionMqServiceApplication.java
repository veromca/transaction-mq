package com.veromca.mqservice;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 可靠性消息服务
 * 消息队列实现最终一致性分布式事务解决方案
 *
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableEurekaClient
@EnableTransactionManagement
@MapperScan("com.veromca.mqservice.mapper")
public class TransactionMqServiceApplication {
	public static void main(String[] args) {
		 SpringApplication.run(TransactionMqServiceApplication.class, args);
	}
}
