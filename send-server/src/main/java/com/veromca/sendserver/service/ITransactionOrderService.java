package com.veromca.sendserver.service;

import com.veromca.sendserver.po.TransactionOrderPO;

/**
 * 消息业务类
 *
 * @author veromca
 */
public interface ITransactionOrderService {

    /**
     * 修改消息状态
     *
     * @param message
     * @return
     */
    boolean updateOrderStatus(TransactionOrderPO message);

}