package com.veromca.sendserver.enums;

/**
 * 消息状态枚举类
 *
 * @author veromca
 */
public enum TransactionOrderStatusEnum {
    /**
     * 待确认
     */
    TO_BE_CONFIRMED(0),

    /**
     * 待收货
     */
    TO_RECEIVED(1),

    /**
     * 已收货
     */
    RECEIVED(2);

    private int status;

    public static TransactionOrderStatusEnum parse(int status) {
        for (TransactionOrderStatusEnum stat : values()) {
            if (stat.getStatus() == status) {
                return stat;
            }
        }
        return null;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    private TransactionOrderStatusEnum(int status) {
        this.status = status;
    }
}