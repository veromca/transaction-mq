package com.veromca.sendserver.mapper;

import com.veromca.sendserver.po.TransactionOrderPO;
import tk.mybatis.mapper.common.Mapper;

/**
 * 
 * 
 * @author veromca
 * @email 1395532033@qq.com
 * @date 2019-12-01 15:30:01
 */
public interface TransactionOrderMapper extends Mapper<TransactionOrderPO> {
	public int retrySendDieMessage();
}
