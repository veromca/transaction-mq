package com.veromca.sendserver.controller;

import com.veromca.sendserver.po.TransactionOrderPO;
import com.veromca.sendserver.service.ITransactionOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 修改订单状态
 *
 */
@RestController
@RequestMapping(value = "/order")
public class TransactionOrderController {

    @Autowired
    private ITransactionOrderService transactionOrderService;

    @PostMapping("/update")
    public boolean updateOrderStatus(@RequestBody TransactionOrderPO order) {
        return transactionOrderService.updateOrderStatus(order);
    }
}
