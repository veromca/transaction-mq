package com.veromca.sendserver;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.transaction.annotation.EnableTransactionManagement;
/**
 * 消息发起服务
 *
 */
@EnableDiscoveryClient
@EnableEurekaClient
@EnableFeignClients(basePackages = "com.veromca.mqclient")
@SpringBootApplication
@EnableTransactionManagement
@MapperScan("com.veromca.sendserver.mapper")
public class SendServiceApplication {
	public static void main(String[] args) {
		 SpringApplication.run(SendServiceApplication.class, args);
	}
}
