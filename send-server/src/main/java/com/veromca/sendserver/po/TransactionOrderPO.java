package com.veromca.sendserver.po;

import com.veromca.sendserver.enums.TransactionOrderStatusEnum;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;


/**
 * @author veromca
 * @email 1395532033@qq.com
 * @date 2019-12-01 15:30:01
 */
@Table(name = "transaction_order")
public class TransactionOrderPO implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    private Integer id;

    /**
     * 消息内容
     */
    @Column(name = "order_no")
    private String orderNo;

    /**
     * 状态：0待确认  1待收货，2已收货
     */
    @Column(name = "order_status")
    private Integer orderStatus = TransactionOrderStatusEnum.RECEIVED.getStatus();

    /**
     * 设置：
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取：
     */
    public Integer getId() {
        return id;
    }

    /**
     * 设置：状态：0待确认  1待收货，2已收货
     */
    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    /**
     * 获取：状态：0待确认  1待收货，2已收货
     */
    public Integer getOrderStatus() {
        return orderStatus;
    }


    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }
}
