CREATE TABLE `transaction_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_no` varchar(30) NOT NULL COMMENT '订单编号',
  `order_status` int(2) NOT NULL COMMENT '状态：0待确认  1待收货，2已收货',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT = '订单';
INSERT INTO transaction_order` (`id`, `order_no`, `order_status`) VALUES ('1', 'N1001', '1');
